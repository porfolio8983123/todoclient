/** @type {import('tailwindcss').Config} */
import { fontFamily } from "tailwindcss/defaultTheme";
const withMT = require("@material-tailwind/react/utils/withMT");

module.exports = withMT({
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        color: {
          1:"#FF694A",
          2:"#11BD23",
          3: "#343434",
          4: "#DDDDDD"
        }
      },
      fontFamily: {
        sans: ["var(--font-sora)", ...fontFamily.sans],
        code: "var(--font-code)",
        grotesk: "var(--font-grotesk)",
      },
      backgroundImage: {
        'dashboard': "url('./src/assets/dashboard.png')",
      }
    },
  },
  plugins: [],
})
