import React, {useState,useEffect} from 'react';
import TodoCard from './TodoCard';
import { useFetchTaskQuery } from '../store';

const Incompleted = () => {

    const {data,refetch} = useFetchTaskQuery();

    const [dataToDisplay,setDataToDisplay] = useState([]);

    useEffect(() => {

        const userEmail = JSON.parse(localStorage.getItem('user_data')).user.email;

        const filterdData = data?.data.filter((item) => item.owner === userEmail || item.assignee === userEmail);
        console.log("filtered data ",filterdData);

        setDataToDisplay(filterdData);

    },[data])

  return (
    <div className='mt-10 mx-6 md:mx-16'>
        <div className='flex justify-between items-center mb-6'>
            <h1 className='text-2xl text-black'>Incompleted</h1>
        </div>
        <div className='flex flex-wrap justify-center md:justify-start'>
            {dataToDisplay && dataToDisplay.map((task,index) => {
                if (task.isCompleted === false) {
                    return (
                        <TodoCard id = {task._id} task = {task.taskName} description = {task.description} dueDate = {task.dueDate} isCompleted={task.isCompleted} assignee={task.assignee} createdAt={task.createdAt} isImportant = {task.isImportant}/>
                    )
                }
            })
            }
        </div>
    </div>
  )
}

export default Incompleted