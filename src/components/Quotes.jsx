import React from 'react'

const Quotes = () => {
  return (
    <div data-aos = "zoom-in" data className='max-w-[600px] text-center' style={{fontStyle:'italic'}}>
        "Efficient task management isn't just about completing to-do lists; it's a journey towards mastering your time, prioritizing your goals, and sculpting your path to success."
    </div>
  )
}

export default Quotes