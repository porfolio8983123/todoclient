import React, {useEffect, useState} from 'react';
import curve from '../assets/curve.png';
import dashboardImage from '../assets/dashboard.png';
import {
  Button,
  Dialog,
  DialogBody,
  DialogFooter,
  Input,
  Typography
} from "@material-tailwind/react";
import Cookies from 'js-cookie';
import useSignup from '../hooks/useSignup.jsx';
import useLogin from '../hooks/useLogin.jsx';
import Quotes from './Quotes.jsx';
import Banner from './Banner.jsx';
import Bento from './Bento.jsx';
import { useAddUserMutation, useFetchUserQuery,useLoginUserMutation } from '../store';

const Landing = ({setLoggingIn}) => {

  const {error,registerUser} = useSignup();
  const {loginError,loginToSystem} = useLogin();

  const {data,isLoading,isError} = useFetchUserQuery();
  const [addUser] = useAddUserMutation();
  const [loginUser] = useLoginUserMutation();

  useEffect(() => {
    console.log("user data ",data);
  })

  const [username,setUsername] = useState('');
  const [email,setEmail] = useState('');
  const [password,setPassword] = useState('');
  const [confirmPassword,setConfirmPassword] = useState('');

  const [open, setOpen] = useState(false);
  const [loginOpen,setLoginOpen] = useState(false);
 
  const handleOpen = () => setOpen(!open);
  const handleLoginOpen = () => setLoginOpen(!loginOpen);

  const handlesignup = () => {
    const data = {
      name:username,
      email,
      password,
      passwordConfirm:confirmPassword
    }

    registerUser(data);
  }

  const handleLogin = () => {
    const data = {
      email,
      password
    }

    loginToSystem(data);
    
  }

  console.log(username);

  return (
   <div>
     <div className="flex justify-center h-screen w-full mt-10 md:mt-0 overflow-hidden">
        <div className='flex flex-col relative justify-between mx-4 md:flex-row md:items-center md:justify-evenly w-full'>
        <div className="flex flex-col justify-between text-center h-[70%]">
            <h1 className="text-4xl font-bold md:text-left"><span className="text-color-1">Your</span>Task</h1>

            <div >
            <h1 className="text-5xl font-bold md:text-left" data-aos = "fade-up" data-aos-delay = "500"><span className="text-color-1">Free</span> <br/> Task Tracking <span className='inline-block relative'>System {" "}
                <img src={curve} alt="" 
                className="absolute top-full left-0 mt-1 w-full xl:-mt-2"
                width={624}
                height={28}
                data-aos="fade-up"
                data-aos-delay = "500"
                />
            </span>
            
            </h1>
            </div>

            <div className='h-[300px] w-[300px] bg-gradient-to-r from-color-1 to-white rounded-full
              absolute top-0 left-[20%] blur-2xl animated-wrapper'>
                  
              </div>

            <div className="space-x-5 md:text-left">
            <button className="bg-color-1 px-8 py-2 rounded-full" onClick={handleOpen}
              data-aos = "fade-up"
              data-aos-delay = "700"
            >Register</button>
            <button className="bg-color-1 px-12 py-2 rounded-full" onClick={handleLoginOpen}
              data-aos = "fade-up"
              data-aos-delay = "750"
            >Login</button>
            </div>
        </div>
        <div className="relative hidden md:block"
          data-aos = "fade-up"
          data-aos-offset = "0"
        >
            <img src={dashboardImage} alt="" width={600} />
            <img className="absolute -bottom-20 -right-20" src={dashboardImage} alt="" width={300} />
        </div>
        </div>

        <Dialog open={open} handler={handleOpen} size='xs' className='relative'>
          <DialogBody className='text-center'>
          <form className="mt-4 mb-2 px-10">
            <div className="mb-1 flex flex-col gap-4">
              <Typography variant="h6" color="blue-gray" className="-mb-3 text-left">
                username
              </Typography>
              <Input
                size="lg"
                placeholder="Dorji Lethro"
                className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
                labelProps={{
                  className: "before:content-none after:content-none",
                }}
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
                value={username}
              />
              <Typography variant="h6" color="blue-gray" className="-mb-3 text-left">
                Email
              </Typography>
              <Input
                size="lg"
                placeholder="name@mail.com"
                className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
                labelProps={{
                  className: "before:content-none after:content-none",
                }}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                value={email}
              />
              <Typography variant="h6" color="blue-gray" className="-mb-3 text-left">
                Password
              </Typography>
              <Input
                type="password"
                size="lg"
                placeholder="********"
                className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
                labelProps={{
                  className: "before:content-none after:content-none",
                }}
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                value={password}
              />

              <Typography variant="h6" color="blue-gray" className="-mb-3 text-left">
                Confirm Password
              </Typography>
              <Input
                type="password"
                size="lg"
                placeholder="********"
                className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
                labelProps={{
                  className: "before:content-none after:content-none",
                }}
                onChange={(e) => {
                  setConfirmPassword(e.target.value);
                }}

                value={confirmPassword}
              />
            </div>
          </form>
          </DialogBody>
          <DialogFooter className='text-center flex justify-center items-center'>
            <Button className='rounded-full bg-color-1'
              onClick={handlesignup}
            >
              <span>Register</span>
            </Button>
          </DialogFooter>

          <div className={`${error ? "absolute":"hidden"} -top-7 rounded-t-xl text-center w-full bg-white text-red-600`}>
            <h4>{error}</h4>
          </div>
        </Dialog>

        {/* login */}

        <Dialog open={loginOpen} handler={handleLoginOpen} size='xs' className='relative'>
          <DialogBody className='text-center'>
          <form className="mt-4 mb-2 px-10">
            <div className="mb-1 flex flex-col gap-4">
              <Typography variant="h6" color="blue-gray" className="-mb-3 text-left">
                Email
              </Typography>
              <Input
                size="lg"
                placeholder="name@mail.com"
                className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
                labelProps={{
                  className: "before:content-none after:content-none",
                }}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                value={email}
              />
              <Typography variant="h6" color="blue-gray" className="-mb-3 text-left">
                Password
              </Typography>
              <Input
                type="password"
                size="lg"
                placeholder="********"
                className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
                labelProps={{
                  className: "before:content-none after:content-none",
                }}
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                value={password}
              />
            </div>
          </form>
          </DialogBody>
          <DialogFooter className='text-center flex justify-center items-center'>
            <Button className='rounded-full bg-color-1'
              onClick={handleLogin}
            >
              <span>Login</span>
            </Button>
          </DialogFooter>

          <div className={`${loginError ? "absolute":"hidden"} -top-7 rounded-t-xl text-center w-full bg-white text-red-600`}>
            <h4>{loginError}</h4>
          </div>
        </Dialog>

    </div>

    <div className='flex justify-center items-center text-center'>
      <Quotes />
    </div>

    <div className='py-10'>
      <Banner />
    </div>

    <div className='px-20 mb-10'>
      <h2 className='text-center mb-3 text-xl'>Why Choose Us?</h2>
      <Bento />
    </div>
   </div>
  )
}

export default Landing