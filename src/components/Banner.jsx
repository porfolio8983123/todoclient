import React from 'react';
import Lottie from 'lottie-react'
import task from '../assets/task.json';

const Banner = () => {
  return (
    <div className='px-10 sm:px-20 py-20 sm:py-20 mt-10'>
        <div className='grid grid-cols-1 place-items-center sm:grid-cols-2 space-y-10 sm:space-x-10'>
            {/* Image */}
            <Lottie animationData={task} className='w-[70%]' data-aos = "fade-up" data-aos-offset = "0"/>
            {/* Text */}
            <div className='space-y-3 text-center md:text-left'>
                <h4 className='text-2xl font-semibold bg-clip-text text-transparent bg-gradient-to-r from-color-1 to-white' data-aos = "fade-up" data-aos-delay = "300">Assigning Task</h4>
                <p className='text-md' data-aos = "fade-up" data-aos-delay = "500">"it's a journey towards mastering your time, prioritizing your goals, and sculpting your path to success."</p>
                <div>
                    <button
                        data-aos = "fade-up" data-aos-delay = "700"
                        className='rounded-full px-6 py-2 bg-gradient-to-r from-color-1 to-black hover:scale-110 transition-all duration-300 hover:bg-gradient-to-l'
                    >Demo</button>
                    
                </div>
            </div>
        </div>
    </div>
  )
}

export default Banner