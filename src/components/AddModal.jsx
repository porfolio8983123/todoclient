import React, { useEffect, useState } from 'react';
import {
    Button,
    Dialog,
    DialogBody,
    DialogFooter,
    Input,
    DialogHeader,
    Textarea 
  } from "@material-tailwind/react";
import { MdCheckCircleOutline } from "react-icons/md";
import { FaCalendarAlt } from "react-icons/fa";
import { FaUserClock } from "react-icons/fa6";
import DatePicker from "react-datepicker";
import Select from 'react-select';
import { useFetchUserQuery } from '../store';
import { useAddTaskMutation,useFetchTaskQuery } from '../store';

import "react-datepicker/dist/react-datepicker.css";

const AddModal = ({open,handleOpen,addingTask}) => {

    const {data,isLoading,isError} = useFetchUserQuery();
    const {data:tasks} = useFetchTaskQuery();
    const [addTask] = useAddTaskMutation();

    const [startDate, setStartDate] = useState(new Date());
    const [selectedOption,setSelectedOption] = useState(null);
    const [options,setOptions] = useState([]);
    const [taskName,setTaskName] = useState('');
    const [description,setDescription] = useState('');

    const [addError,setAddError] = useState('');
    const [taskAdded,setTaskAdded] = useState(false);

    function handleChange(selectedOption){
        setSelectedOption(selectedOption);
    };

    useEffect(() => {
        let option = [];
        if (data) {
            data.data.forEach(item => {
                option.push({
                    value: item.email,
                    label: item.name
                })
            }) 
        }

        setOptions(option);
    },[data])

    useEffect(() => {
        console.log("user ",JSON.parse(localStorage.getItem("user_data")).user.email)
    },[])

    const handleTaskAdd = () => {
        const data = {
            taskName,
            description,
            dueDate:startDate,
            assignee: selectedOption?.value,
            owner:JSON.parse(localStorage.getItem("user_data")).user.email
        }

        addTask(data)
        .unwrap()
        .then((response) => {
            console.log("response from add task ",response);
            setAddError('');
            setTaskAdded(true);
            addingTask();
        })
        .catch((error) => {
            setTaskAdded(false);
            console.log(error);
            setAddError(error.data?.err);
            addingTask();

        })
    }

  return (
    <Dialog open={open} handler={handleOpen} size='xs' className='relative'>
        <DialogHeader className='flex justify-center items-center'><h4>Adding Task</h4></DialogHeader>
        <div className={`${addError ? "block":"hidden"} text-center text-red-600`}>{addError}</div>
        <div className={`${taskAdded ? "block":"hidden"} text-center text-green-600`}>Successfully Created the Task!</div>
        <DialogBody className='text-center'>
        <form className="mt-4 mb-2 px-10">
        <div className="mb-1 flex flex-col gap-4">
            <div className='flex items-center'>
                <MdCheckCircleOutline size={32} className='mr-3'/>
                <Input
                size="lg"
                placeholder="Write a task name"
                className="border-none !border-t-blue-gray-200 focus:!border-t-gray-900"
                labelProps={{
                    className: "before:content-none after:content-none",
                }}
                value={taskName}
                onChange={(e) => {
                    setTaskName(e.target.value);
                }}
                />
            </div>
            <Textarea label="Description" 
                value={description}
                onChange={(e) => {
                    setDescription(e.target.value);
                }}
            />
            
            <div className='space-y-6'>
                <div className='flex justify-start items-center space-x-10'>
                    <FaCalendarAlt size={25}/>
                    <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} className='bg-white'/>
                </div>
                <div className='flex items-center space-x-10'>
                    <FaUserClock size={32}/>
                    <Select
                        value={selectedOption}
                        onChange={handleChange}
                        options={options}
                    />
                </div>
            </div>
        </div>
        </form>
        </DialogBody>
        <DialogFooter className='text-center flex justify-center items-center'>
        <Button className='rounded-full bg-color-1'
        onClick={handleTaskAdd}
        >
            <span>Add</span>
        </Button>
        </DialogFooter>
    </Dialog>
  )
}

export default AddModal