import React, { useEffect, useState } from 'react';
import TodoCard from './TodoCard';
import { MdAdd } from "react-icons/md";
import AddModal from './AddModal';
import { useFetchTaskQuery } from '../store';

const AllTask = () => {

    const { data, refetch } = useFetchTaskQuery();

    const [allData, setAllData] = useState([]);
    const [dataToDisplay, setDataToDisplay] = useState([]);
    const [searchText, setSearchText] = useState('');

    const [open, setOpen] = useState(false);
    const [taskAdded, setTaskAdded] = useState(false);

    const handleOpen = () => setOpen(!open);

    const addingTask = () => {
        setTaskAdded(!taskAdded);
    }

    useEffect(() => {
        const userEmail = JSON.parse(localStorage.getItem('user_data')).user.email;
        const filteredData = data?.data.filter((item) => item.owner === userEmail || item.assignee === userEmail);
        setAllData(filteredData);
        setDataToDisplay(filteredData); // Initially set dataToDisplay to filteredData
    }, [data])

    useEffect(() => {
        if (searchText) {
            const filteredTasks = allData.filter(task =>
                task.taskName.toLowerCase().includes(searchText.toLowerCase()) ||
                task.description.toLowerCase().includes(searchText.toLowerCase())
            );
            setDataToDisplay(filteredTasks);
        } else {
            setDataToDisplay(allData); // Reset dataToDisplay to allData when searchText is empty
        }
    }, [searchText, allData])

    console.log("search text ", searchText);

    return (
        <div className='mt-10 mx-6 md:mx-16'>
            <div className='flex justify-between items-center mb-6'>
                <h1 className='text-2xl text-black'>All Tasks</h1>
                <div class="relative flex">
                    <input
                        type="search"
                        class="relative m-0 block flex-auto rounded border border-solid border-black bg-transparent bg-clip-padding px-3 py-[0.25rem] text-black font-normal leading-[1.6] text-surface outline-none transition duration-200 ease-in-out placeholder:text-black focus:z-[3] focus:border-primary focus:shadow-inset focus:outline-none motion-reduce:transition-none dark:border-white/10 dark:text-white dark:placeholder:text-neutral-200 dark:autofill:shadow-autofill dark:focus:border-primary"
                        placeholder="Search"
                        aria-label="Search"
                        id="exampleFormControlInput2"
                        value={searchText}
                        onChange={(e) => {
                            setSearchText(e.target.value);
                        }}
                        aria-describedby="button-addon2" />
                    <span
                        class="flex items-center whitespace-nowrap px-3 py-[0.25rem] text-black dark:border-neutral-400 dark:text-white [&>svg]:h-5 [&>svg]:w-5"
                        id="button-addon2">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke-width="2"
                            stroke="currentColor">
                            <path
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                d="m21 21-5.197-5.197m0 0A7.5 7.5 0 1 0 5.196 5.196a7.5 7.5 0 0 0 10.607 10.607Z" />
                        </svg>
                    </span>
                </div>
                <div className='border h-[30px] w-[30px] rounded-full bg-[#768396] flex justify-center items-center mr-8 md:mr-16 cursor-pointer'
                    onClick={handleOpen}
                >
                    <MdAdd size={30} />
                </div>

                <AddModal open={open} handleOpen={handleOpen} addingTask={addingTask} />
            </div>
            <div className='flex flex-wrap justify-center md:justify-start'>
                {dataToDisplay && dataToDisplay.map((task, index) => (
                    <TodoCard key={task._id} id={task._id} task={task.taskName} description={task.description} dueDate={task.dueDate} isCompleted={task.isCompleted} assignee={task.assignee} createdAt={task.createdAt} isImportant={task.isImportant} addingTask={addingTask} />
                ))
                }
            </div>
        </div>
    )
}

export default AllTask;
