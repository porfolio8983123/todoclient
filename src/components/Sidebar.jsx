import React, { useEffect, useState } from 'react';
import {Outlet,useParams,useLocation,useNavigate} from 'react-router-dom';
import { FaArrowRight } from "react-icons/fa";
import { FaArrowLeft } from "react-icons/fa";
import { TbSubtask } from "react-icons/tb";
import { MdLabelImportant } from "react-icons/md";
import { BiCheckCircle } from "react-icons/bi";
import { MdIncompleteCircle } from "react-icons/md";
import { MdGroupAdd } from "react-icons/md";
import { LiaSignOutAltSolid } from "react-icons/lia";
import Cookies from 'js-cookie';
import { useAuth } from '../contexts/AuthContext.jsx';
import maple from '../assets/bg.jpg';
import {useDispatch, useSelector} from 'react-redux';
import { updateNavigation } from '../store/index.jsx';

const navigation = [
  {
    id: "0",
    title: "All Task",
    url: "#alltask",
    path: "/todo",
    icon:<TbSubtask />
  },
  {
    id: "1",
    title: "important",
    url: "todo/important",
    path:"important",
    icon: <MdLabelImportant />
  },
  {
    id: "2",
    title: "Completed",
    url: "todo/completed",
    icon: <BiCheckCircle />,
    path:"completed",
  },
  {
    id: "3",
    title: "Incompleted",
    url: "todo/incompleted",
    icon: <MdIncompleteCircle />,
    path:"incompleted",
  },
  {
    id: "4",
    title: "Members",
    url: "todo/members",
    icon: <MdGroupAdd />,
    path:"members",
  }
];


const Sidebar = ({setLoggingIn}) => {

  const dispatch = useDispatch();

  const {value} = useSelector((state) => state.navigation);

  console.log("sidelin value ",value);

  const {userData} = useAuth();

  console.log("user data ", userData)

  const {logout} = useAuth();

  const {pathname} = useLocation();
  const navigate = useNavigate();

  const [path,setPath] = useState(pathname);

  const [openNavigation,setOpenNavigation] = useState(false);

  const handleLogout = () => {
    logout();
  }

  return (
   <div className=''>
      <div className='relative flex mx-6 my-6 md:mx-20 md:my-10 md:flex-row'>
        <div className='bg-[#343434] hidden md:block bg-3 w-[250px] border h-[610px] rounded-xl'>
        <div className='flex flex-col h-full justify-between items-center'>
            {/* user profile */}
            <div className='flex mt-10 items-center'>
              <div className='border h-12 w-12 rounded-full'>
                <img className='h-full w-full rounded-full' src="https://th.bing.com/th/id/OIP.bvWxUAdkIDiwWHlaCX-ArAAAAA?rs=1&pid=ImgDetMain" alt="" />
              </div>
              <h4 className='ml-2 text-sm'>{userData?.name}</h4>
            </div>

            {/* navigation */}

            <div className='flex flex-col space-y-3 w-full text-center'>
              {navigation && navigation.map((item,index) => (
                 <div key={item.id} className={`cursor-pointer w-full py-2 ${index === value ? "bg-[#666666] border-r-[10px] border-r-green-600":""}`}
                    onClick={() => {
                      dispatch(updateNavigation(index));
                      navigate(item.path);
                      setOpenNavigation(false);
                    }}
                  >
                    <div className='flex items-center ml-8 text-left'>
                      {item.icon} 
                      <a className='ml-3'>{item.title}</a>
                    </div>
                  </div>
              ))
              }
            </div>

            {/* sign out */}
            <div className='flex items-center space-x-5 pb-10 -ml-5'>
              <LiaSignOutAltSolid size={25}/> 
              <button
                onClick={handleLogout}
              >Sign Out</button>
            </div>
          </div>
        </div>
        {!openNavigation &&
          <FaArrowRight size={25} onClick={() => {
            setOpenNavigation(true);
          }} className='animate-bounce md:hidden'/>
        }

        {/* small screen */}

        <div className={`${openNavigation? "absolute":"hidden"} md:hidden -left-5 z-100 bg-3 w-[200px] border border-r-4 border-r-orange-600 h-[610px] rounded-xl bg-black shadow-xl`}>
          <div className='relative'>
            {openNavigation &&
              <FaArrowLeft size={25} 
                onClick={() => {
                  setOpenNavigation(false)
                }}
                className='animate-bounce absolute right-3 top-3'
              />
            }
          </div>
          
          <div className='flex flex-col h-full justify-between items-center'>
            {/* user profile */}
            <div className='flex mt-10 items-center'>
              <div className='border h-12 w-12 rounded-full'>
                <img className='h-full w-full rounded-full' src="https://th.bing.com/th/id/OIP.bvWxUAdkIDiwWHlaCX-ArAAAAA?rs=1&pid=ImgDetMain" alt="" />
              </div>
              <h4 className='ml-2 text-sm'>{userData?.name}</h4>
            </div>

            {/* navigation */}

            <div className='flex flex-col space-y-3 w-full text-center'>
              {navigation && navigation.map((item) => (
                <div key={item.id} className={`cursor-pointer w-full py-2 ${pathname === item.path ? "bg-[#666666] border-r-[10px] border-r-green-600":""}`}
                  onClick={() => {
                    navigate(item.path);
                    setOpenNavigation(false)
                  }}
                >
                  <div className='flex items-center ml-8 text-left'>
                    {item.icon} 
                    <a className='ml-3'>{item.title}</a>
                  </div>
                </div>
              ))
              }
            </div>

            {/* sign out */}
            <div className='flex items-center space-x-5 pb-10'>
              <LiaSignOutAltSolid size={25}/> 
              <button
                onClick={handleLogout}
              >Sign Out</button>
            </div>
          </div>
        </div>
        <main className='mainContent bg-[#343434] border ml-5 md:ml-10 w-full rounded-xl h-[610px] overflow-auto' 
        >
          <Outlet />
        </main>
      </div>
   </div>
  )
}

export default Sidebar