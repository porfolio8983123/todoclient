import React, { useState } from 'react';
import {
  Input,
  Button
} from "@material-tailwind/react";
import { useFetchUserQuery, useDeleteUserMutation, useSendMailMutation } from '../store';

const Member = () => {

  const {data,isLoading,isError} = useFetchUserQuery();

  const [deleteUser] = useDeleteUserMutation();
  const [sendMail,{isLoading:sendLoading}] = useSendMailMutation();

  const [error,setError] = useState('');
  const [email,setEmail] = useState('');

  const handleUserDelete = (id) => {
    console.log(id)
    deleteUser({id})
    .unwrap()
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log(error);
    })
  }

  const handleSendMail = () => {
    const data = {
      email
    }

    sendMail(data)
    .unwrap()
    .then((response) => {
      console.log(response);
      setError('');
    })
    .catch((error) => {
      console.log(error);
      setError(error?.data?.err)
    })
  }

  const owner = JSON.parse(localStorage.getItem('user_data')).user.email;

  return (
    <div className='mt-10 mx-6 md:mx-16'>
      <div className='flex justify-between items-center mb-6'>
          <h1 className='text-2xl text-black'>Members</h1>
          <div>
            <form className="mt-8 mb-2 w-80 max-w-screen-lg sm:w-96">
              <div className="mb-1 flex items-center gap-6">
                <div className='relative'>
                  <Input
                    size="lg"
                    placeholder="Envite by email"
                    className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
                    labelProps={{
                      className: "before:content-none after:content-none",
                    }}
                    value={email}
                    onChange={(e) => {
                      setEmail(e.target.value);
                    }}
                  />
                 {error ? (
                   <span className='absolute text-red-700'>{error}</span>
                 ):(
                  <span className='absolute text-green-700'>Successfully sent!</span>
                 )

                 }
                </div>
              <Button className="bg-color-1" fullWidth disabled = {sendLoading}
                onClick={handleSendMail}
              >
                Invite
              </Button>
              </div>
            </form>
          </div>
      </div>
      <div className='space-y-6 py-3'>
        {data?.data && data?.data.map((user) => (
          <div key={user._id} className='flex justify-between bg-gray-400 py-3 px-2 rounded-xl'>
            <div className='flex space-x-4 items-center '>
              <div className='border p-5 rounded-full bg-[#D9D9D9] text-black w-[50px] h-[50px] flex justify-center items-center'>
                {user.name.charAt(0)}
              </div>
              <div className='flex flex-col'>
                <h4 className='text-black'>
                  {user.name}
                </h4>
                <h6 className="text-gray-600">
                  {user.email}
                </h6>
              </div>
            </div>

            <div className={`flex justify-center items-center mr-10 ${user.email === owner ? "hidden":""}`}>
              <button className='rounded-xl bg-color-1 px-4 py-2 hover:bg-gradient-to-r from-color-1 to-black'
                onClick={() => {
                  handleUserDelete(user._id);
                }}
              >Remove</button>
            </div>
          </div>
        ))

        }
      </div>
    </div>
  )
}

export default Member