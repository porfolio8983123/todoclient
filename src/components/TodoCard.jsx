import React, {useState} from 'react';
import { FiEdit } from "react-icons/fi";
import { MdDelete } from "react-icons/md";
import { useDeleteTaskMutation, useUpdateImportantMutation, useUpdateCompleteMutation } from '../store';
import EditModal from './EditModal';

const TodoCard = ({id,task,description,dueDate,isCompleted,assignee,createdAt,isImportant, addingTask}) => {

    console.log("important ",isImportant);
    const [deleteTask] = useDeleteTaskMutation();
    const [updateImportant] = useUpdateImportantMutation();
    const [updateComplete] = useUpdateCompleteMutation();

    const [open,setOpen] = useState(false);

    const handleOpen = () =>setOpen(!open);
    
    const handleDelete = (id) => {
        deleteTask({id})
        .unwrap()
        .then((response) => {
            console.log("delete ",response);
            addingTask();
        })
        .catch((error) => {
            console.log(error);
            addingTask();
        })
    }

    const formatDate = (dateString) => {
        const options = {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        return new Date(dateString).toLocaleString('en-US', options);
    };

    const handleImportantChange = () => {
        console.log("important change");
        const data = {
            value: isImportant?false:true
        }

        updateImportant({id,data})
        .unwrap()
        .then((response) => {
            console.log("response from update ",response);
            addingTask();
        })
        .catch((error) => {
            console.log(error);
            addingTask();
        })
    }

    const handleUpdateComplete = () => {
        const data = {
            value: isCompleted ? false:true
        }

        updateComplete({id,data})
        .unwrap()
        .then((response) => {
             addingTask();
        })
        .catch((error) => {
            addingTask();
        })
    }

  return (
    <div className='rounded-xl bg-[#4F4F4F] min-w-[310px] mb-3 mr-3 hover:bg-gradient-to-r from-[#ffc3b6] to-white hover:text-black
    transition-all hover:scale-90 duration-800'>
        <div className='my-4 mx-4'>
            <div className='flex items-center justify-between'>
                <h1 className='text-xl font-bold'>{task}</h1>
                <div className='flex items-center space-x-2'>
                    <p className='text-xs'>important?</p>
                    <input type="checkbox" checked={isImportant}
                        onChange={handleImportantChange}
                    />
                </div>
            </div>
            <p className='text-sm text-slate-400 mb-12 mt-3'>{description}</p>

            <h3 className='text-md text-slate-400'>{formatDate(dueDate)}</h3>
            <div className='flex justify-between items-center'>
                {isCompleted ? (
                    <button className='bg-green-500 rounded-full px-4 py-1 mt-2'
                        onClick={handleUpdateComplete}
                    >Completed</button>
                ):(
                    <button className='bg-color-1 rounded-full px-4 py-1 mt-2'
                        onClick={handleUpdateComplete}
                    >Incomplete</button>
                )

                }
                <div className='flex items-center space-x-3'>
                    <FiEdit size={25}
                        onClick={handleOpen}
                    />
                    <MdDelete size={28} 
                        onClick={() => {
                            handleDelete(id);
                        }}
                        className='cursor-pointer'
                    />
                </div>

                <EditModal id = {id} task = {task} taskdescription = {description} dueDate = {dueDate} assignee = {assignee} open = {open} handleOpen={handleOpen} addingTask = {addingTask}/>
            </div>
        </div>
    </div>
  )
}

export default TodoCard