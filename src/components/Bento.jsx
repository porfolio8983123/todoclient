import React from 'react';
import Lottie from 'lottie-react';
import add from '../assets/add.json';
import addTask from '../assets/addTask.jpg';
import assign from '../assets/assignTask.jpg';
import { MdOutlineAssignmentInd } from "react-icons/md";
import { FcInvite } from "react-icons/fc";
import invite from '../assets/invite.jpg';
import { CiCalendarDate } from "react-icons/ci";
import due from '../assets/due.jpg';

const FeaturesData = [
    {
      name: "Adding Task",
      icon: (
        <MdOutlineAssignmentInd className="text-5xl text-color-1 duration-300" />
      ),
      link: "#",
      description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit.",
      aosDelay: "300",
    },
    {
      name: "Assign Task",
      icon: (
        <MdOutlineAssignmentInd className="text-5xl text-color-1  duration-300" />
      ),
      link: "#",
      description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit.",
      aosDelay: "500",
    },
    {
      name: "Invite",
      icon: (
        <FcInvite className="text-5xl text-color-1 duration-500" />
      ),
      link: "#",
      description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit.",
      aosDelay: "700",
    },
    {
        name: "Due Date",
        icon: (
          <CiCalendarDate className="text-5xl text-color-1  duration-500" />
        ),
        link: "#",
        description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit.",
        aosDelay: "700",
      }
  ];


const Bento = () => {
  return (
    <div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-10'>
        {FeaturesData.map((data,index) => (
            <div key={index} data-aos = "fade-up" data-aos-delay = {data.aosDelay} className='bg-white text-center group space-y-3 sm:space-y-6 p-4 sm:py-10 bg-dark hover:bg-gradient-to-r from-primary to-secondary
            hover:shadow-[0_0_40px_#007cfff0] text-black hover:bg-black hover:text-white rounded-lg duration-300'>
                <div className='grid place-items-center'>{data.icon}</div>
                <h1 className='text-xl'>{data.name}</h1>
                <p>{data.description}</p>
                <a href={data.link}
                    className='inline-block text-lg font-semibold py-3 text-primary group-hover:text-black duration-300'
                >Learn More</a>
            </div>
        ))

        }
    </div>
  )
}

export default Bento