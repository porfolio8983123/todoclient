import {createSlice} from '@reduxjs/toolkit';

const navigationSlice = createSlice({
    name:'navigation',
    initialState: {
        value: 0
    },
    reducers: {
        updateNavigation: (state,action) => {
            state.value = action.payload
        }
    }
})

export const {updateNavigation} = navigationSlice.actions;
export const navigationReducer = navigationSlice.reducer;