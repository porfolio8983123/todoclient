import { createSlice } from "@reduxjs/toolkit";

const sideLineSlice = createSlice({
    name:'taskData',
    initialState: {
        data:[]
    },
    reducers: {
        updateSideLine: (state,action) => {
            state.data = action.payload;
        }
    }
})

export const {updateSideLine} = sideLineSlice.actions;
export const sideLineReducer = sideLineSlice.reducer;