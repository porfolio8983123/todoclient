import {createApi,fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const userApi = createApi({
    reducerPath: 'user',
    tagTypes: ['users'],
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:3000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization', `Bearer ${token}`);
            return headers;
        },
    }),
    endpoints(builder) {
        return {
            fetchUser: builder.query({
                query: () => {
                    return {
                        url: '/',
                        method:'GET'
                    }
                },
                providesTags:['users']
            }),
            addUser: builder.mutation({
                query: (data) => {
                    return {
                        url: '/signup',
                        method: 'POST',
                        body: data
                    }
                },
                invalidatesTags:['users']
            }),
            loginUser: builder.mutation({
                query: (data) => ({
                    url: '/login',
                    method:'POST',
                    body:data
                }),
                invalidatesTags:['users']
            }),
            deleteUser: builder.mutation({
                query: ({id}) => ({
                    url: `/${id}`,
                    method: 'DELETE'
                }),
                invalidatesTags:['users']
            }),
            sendMail: builder.mutation({
                query: (data) => ({
                    url: "/mail",
                    method: 'POST',
                    body: data
                })
            })

        }
    }
})

export const {useFetchUserQuery, useAddUserMutation, useLoginUserMutation, useDeleteUserMutation, useSendMailMutation} = userApi;
export {userApi};