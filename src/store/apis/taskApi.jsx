import {createApi,fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const taskApi = createApi({
    reducerPath: 'task',
    tagTypes: ['tasks'],
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:3000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization', `Bearer ${token}`);
            return headers;
        },
    }),
    endpoints(builder) {
        return {
            addTask: builder.mutation({
                query: (data) => {
                    return {
                        url: '/task',
                        method:'POST',
                        body:data
                    }
                },
                invalidatesTags: ['tasks']
            }),
            fetchTask: builder.query({
                query: () => {
                    return {
                        url: '/task',
                        method: 'GET'
                    }
                },
                providesTags: ['tasks']
            }),
            deleteTask: builder.mutation({
                query: ({id}) => {
                    return {
                        url: `/task/${id}`,
                        method: 'DELETE'
                    }
                },
                invalidatesTags: ['tasks']
            }),
            updateImportant: builder.mutation({
                query: ({id,data}) => {
                    return {
                        url:`task/${id}`,
                        method: 'PATCH',
                        body:data
                    }
                },
                invalidatesTags: ['tasks']
            }),
            updateComplete: builder.mutation({
                query: ({id,data}) => {
                    return {
                        url: `task/updateComplete/${id}`,
                        method: 'PATCH',
                        body: data
                    }
                },
                invalidatesTags: ['tasks']
            }),
            updateTask: builder.mutation({
                query: ({id,data}) => {
                    return {
                        url: `task/${id}`,
                        method:'PUT',
                        body:data
                    }
                },
                invalidatesTags: ['tasks']
            })
        }
    }
})

export const {useAddTaskMutation, useFetchTaskQuery, useDeleteTaskMutation,useUpdateImportantMutation, useUpdateCompleteMutation, useUpdateTaskMutation} = taskApi;
export {taskApi};