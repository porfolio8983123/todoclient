import {configureStore} from '@reduxjs/toolkit';
import { userApi } from './apis/userApi';
import { setupListeners } from "@reduxjs/toolkit/query";
import { taskApi } from './apis/taskApi';
import { sideLineReducer, updateSideLine } from './slices/sideLineSlice';
import { navigationReducer,updateNavigation } from './slices/navigationSlice';

export const store = configureStore({
    reducer: {
        [userApi.reducerPath]:userApi.reducer,
        [taskApi.reducerPath]:taskApi.reducer,
        taskData:sideLineReducer,
        navigation:navigationReducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware()
        .concat(userApi.middleware)
        .concat(taskApi.middleware),
})

setupListeners(store.dispatch);

export {useFetchUserQuery, useAddUserMutation, useLoginUserMutation, useDeleteUserMutation, useSendMailMutation} from './apis/userApi';
export {useAddTaskMutation,useFetchTaskQuery, useDeleteTaskMutation,useUpdateImportantMutation, useUpdateCompleteMutation, useUpdateTaskMutation} from './apis/taskApi';
export {
    updateSideLine,
    updateNavigation
}