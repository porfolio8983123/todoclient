export const data = [
    {
        taskName:'Login',
        description: "As a user I want to Login..",
        dueDate: '03/01/2024',
        isCompleted: false,
        assignee: "Pema Tshering",
        createdAt:'03/01/2024',
        isImportant:true
    },
    {
        taskName:'Register',
        description: "As a user I want to Login..",
        dueDate: '03/01/2024',
        isCompleted: true,
        assignee: "Sonam",
        createdAt:'03/01/2024',
        isImportant:true
    },
    {
        taskName:'Add task',
        description: "As a user I want to Login..",
        dueDate: '03/01/2024',
        isCompleted: false,
        assignee: "Pema",
        createdAt:'03/01/2024',
        isImportant:false
    },
    {
        taskName:'Remove task',
        description: "As a user I want to Login..",
        dueDate: '03/01/2024',
        isCompleted: false,
        assignee: "Pema Tshering",
        createdAt:'03/01/2024',
        isImportant:true
    },
    {
        taskName:'Design',
        description: "As a user I want to Login..",
        dueDate: '03/01/2024',
        isCompleted: true,
        assignee: "Jigme Dema",
        createdAt:'03/01/2024',
        isImportant:true
    },
    {
        taskName:'ERD',
        description: "As a user I want to Login..",
        dueDate: '03/01/2024',
        isCompleted: true,
        assignee: "Karma",
        createdAt:'03/01/2024',
        isImportant:false
    },
    {
        taskName:'ERD',
        description: "As a user I want to Login..",
        dueDate: '03/01/2024',
        isCompleted: true,
        assignee: "Karma",
        createdAt:'03/01/2024',
        isImportant:false
    },
    {
        taskName:'ERD',
        description: "As a user I want to Login..",
        dueDate: '03/01/2024',
        isCompleted: true,
        assignee: "Karma",
        createdAt:'03/01/2024',
        isImportant:true
    },
    {
        taskName:'ERD',
        description: "As a user I want to Login..",
        dueDate: '03/01/2024',
        isCompleted: true,
        assignee: "Karma",
        createdAt:'03/01/2024',
        isImportant:true
    },
    {
        taskName:'ERD',
        description: "As a user I want to Login..",
        dueDate: '03/01/2024',
        isCompleted: true,
        assignee: "Karma",
        createdAt:'03/01/2024',
        isImportant:false
    }
]