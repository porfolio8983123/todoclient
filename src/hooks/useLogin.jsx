import { useState } from "react";
import { useAuth } from "../contexts/AuthContext.jsx";
import { useLoginUserMutation } from "../store";

const useLogin = () => {

    const [loginUser] = useLoginUserMutation();

    const {login} = useAuth();

    const [loginError,setLoginError] = useState(null);

    const loginToSystem = async (data) => {
        loginUser(data)
        .unwrap()
        .then((response) => {
            setLoginError(null);
            console.log("response from hook ",response);
            login(response.token,response.user);
        })
        .catch((error) => {
            console.log("error ",error);
            setLoginError(error.data?.err);
        })
    }

  return {loginError,loginToSystem};
}

export default useLogin;