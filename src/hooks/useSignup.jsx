import { useState } from "react";
import { useAuth } from "../contexts/AuthContext.jsx";
import { useAddUserMutation } from "../store";

const useSignup = () => {

    const [addUser] = useAddUserMutation();

    const {login} = useAuth();

    const [error,setError] = useState(null);

    const registerUser = async (data) => {
        addUser(data)
        .unwrap()
        .then((response) => {
            setError(null);
            console.log("response from hook ",response);
            login(response.token,response.user);
        })
        .catch((error) => {
            console.log("error ",error);
            setError(error.data?.err);
        })
    }

  return {error,registerUser};
}

export default useSignup;