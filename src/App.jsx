import Landing from "./components/Landing";
import {BrowserRouter,Routes,Route, Navigate} from 'react-router-dom';
import Sidebar from "./components/Sidebar";
import AllTask from "./components/AllTask";
import Important from "./components/Important";
import Completed from "./components/Completed";
import Incompleted from "./components/Incompleted";
import Member from "./components/Member";
import { useState, useEffect } from "react";
import { useAuth } from "./contexts/AuthContext.jsx";
import AOS from 'aos';
import 'aos/dist/aos.css';

function App() {

  const {isAuthenticated} = useAuth();

  useEffect(() => {
    AOS.init({
      offset: 100,
      duration: 800,
      easing: "ease-in-sine",
      delay: 100
    })
  },[])

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={isAuthenticated ? <Navigate to = "/todo"/>:<Landing />}/>
        <Route path="/todo" element={isAuthenticated ? <Sidebar />: <Navigate to= "/"/>}>
          <Route index element={<AllTask />} />
          <Route path = "important" element={<Important />} />
          <Route path = "completed" element={<Completed />} />
          <Route path = "incompleted" element={<Incompleted />} />
          <Route path = "members" element={<Member />} />
        </Route>

      </Routes>
    </BrowserRouter>
  )
}

export default App
